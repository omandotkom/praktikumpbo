//Belajar perulangan
public class Angka{
    public static void main(String[] args){
        
        int i;
        //dengan for
        for (i=1;i<=10;i++){
            System.out.println(i);
        }
        
        //dengan while
        i=1;
        while (i<=10){
            System.out.println(i);
            i++;
        }

        //dengan do while
        i = 1;
        do{
            System.out.println(i);
            i++;
        }while (i<=10);
    }
}