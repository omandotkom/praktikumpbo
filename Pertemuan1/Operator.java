//Belajar Operator

public class Operator {
    public static void main(String[] args){
    //1. Operator Aritmatika
    int angka1 = 3;
    int angka2 = 4;
    int hasilTambah = angka1 + angka2;
    
    int hasilKurang = angka1 - angka2;
    System.out.println("Hasil tambah " + hasilTambah);
    System.out.println("Hasil kurang " + hasilKurang);

    //2. Operator Assignment

    int contohAngka = 1;
    contohAngka++;
    System.out.println("Hasil contohAngka " + contohAngka);
    

    //3. Bitwise
    int a = 60;	/* 60 = 0011 1100 */
    int b = 13;	/* 13 = 0000 1101 */
    int c = 0;
    c = a & b;        /* 12 = 0000 1100 */
    System.out.println("a & b = " + c );
    c = a | b;        /* 61 = 0011 1101 */
    System.out.println("a | b = " + c );
}
}