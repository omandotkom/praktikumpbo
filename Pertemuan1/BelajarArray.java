//Belajar Array

public class BelajarArray{

    public static void main(String[] args){
        //membuat array dengan tipe data integer
        //kalau jumlahnya 5, berarti 0 sampai 4
        int[] kumpulanAngka = new int[5];

        int[] kumpulanNilai = {5, 3, 23, 99, 22};
        
        //mengisi variabel kumpulanAngka

        kumpulanAngka[0] = 20;
        kumpulanAngka[1] = 30;
        kumpulanAngka[2] = 40;
        kumpulanAngka[3] = 50;
        kumpulanAngka[4] = 60;

        int contohJumlah = kumpulanAngka[1] + kumpulanAngka[3];

        System.out.println("Hasil penjumlahan : " + contohJumlah);
    }
}