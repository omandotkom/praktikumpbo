//Source code untuk bagian 2.6 Keyword "this"

class Manusia{
    private String nama;
    private int umur;

    //coba ini nanti dijadiin private
    public void setNama(String a){
        nama = a;
    }
    public String getNama(){
        return nama;
    }
    public void setUmur(int a){
        umur = a;
    }
    public int getUmur(){
        return umur;
    }

    //tambahkan kode ini : 
    //constructor default tanpa parameter
    public Manusia(){}

    //constructor dengan 1 parameter
    public Manusia(String a){
    nama = a;
    }

    //constructor dengan 2 parameter
    public Manusia(String a, int b){
        nama = a;
        umur = b;
    }

}    
public class DemoManusia{

    public static void main(String[] args){
    Manusia arrMns[] = new Manusia[3]; //buat array bertipe objek
    Manusia objMns1 = new Manusia();
    
    
    objMns1.setNama("Markonah");
    objMns1.setUmur(76);
    
    //mengakses constructor dengan 1 parameter
    Manusia objMns2 = new Manusia("Mat Conan");

    Manusia objMns3 = new Manusia("Bajuri", 13);

    arrMns[0] = objMns1;
    arrMns[1] = objMns2;
    arrMns[2] = objMns3;

    for (int i =0; i<3; i++){
        System.out.println("Nama : "+ arrMns[i].getNama());
        System.out.println("Umur : "+ arrMns[i].getUmur());
        System.out.println();
    }
}

}