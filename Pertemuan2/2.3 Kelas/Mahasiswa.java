//Source code untuk bagian 2.3 Kelas
public class Mahasiswa{
    String nama; //variabel nama bertipe String
    int nim; //variabel nim bertipe integer
    void getNamaMahasiswa(){} //contoh subprogram atau prosedur}
    
    //contoh subprogram berupa function yang mengembalikan nilai integer
    //dari variabel nim
    int getNim(){ 
        nim = 10;
        return nim;
    }
}