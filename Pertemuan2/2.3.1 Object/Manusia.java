//Source code untuk bagian 2.3.1 Objek
public class Manusia{
    public String getNama(){
        return "Nama saya Marbun";
    }
    public static void main(String[] args){
        Manusia objMns1 = new Manusia(); //membuat objek manusia
        String nama = objMns1.getNama(); 
        System.out.println("Nama saya adalah " + nama);

        //membuat objek bernama mahasiswa1 dari kelas Mahasiswa
        Mahasiswa mahasiswa1 = new Mahasiswa();
        //mahasiswa1.getNim() berasal dari function yang tadi dibuat.
        int nim = mahasiswa1.getNim();
        System.out.println("Nim saya " + nim);
    }
}