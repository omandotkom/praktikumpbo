//Source code untuk bagian 2.5 Method
class TestPass{
    int i,j;
    TestPass(int a, int b){
        i = a;
        j = b;
    }

    void calculate(int m, int n){
        m = m * 10;
        n = n / 2;
    }
    void calculate(TestPass e){
        e.i = e.i * 10;
        e.j = e.j / 2;
    }

}
    public class PassedByValue{
        public static void main (String[] args){
            int x,y;
            TestPass z = new TestPass(50, 100);
            x = 10;
            y = 20;

            System.out.println("Nilai sebelum passed By Value : " );
            System.out.println("X = " + x);
            System.out.println("Y = " + y);

            z.calculate(x, y);
            System.out.println("Nilai sesudah passed By Value : " );
            System.out.println("X = " + x);
            System.out.println("Y = " + y);

        
            System.out.println("Nilai sebelum passed By Reference : " );
            System.out.println("z.i = " + z.i);
            System.out.println("z.j = " + z.j);

            z.calculate(z);
            System.out.println("Nilai sesudah passed By Reference : " );
            System.out.println("z.i = " + z.i);
            System.out.println("z.j = " + z.j);

            
        }
    }
