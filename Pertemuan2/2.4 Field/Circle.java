//Source code untuk bagian 2.4 Field & Static

public class Circle{
    public static final double PI = 3.1459;
    public static double radiansToDegrees(double rads){
        return rads * 180 / PI;
    }

    public double r;
    public double area(){
        return PI * r * r;
    }

    public double circumference(){
        return 2 * PI * r;
    }

    public static void main(String[] args){
        Circle lingkaran = new Circle();
        System.out.println("Radian to degrees " + lingkaran.radiansToDegrees(4.2));

        //mencoba static
        System.out.println("PI adalah : " + Circle.PI);
        //pada kode di atas, karna variabel PI itu menggunakan
        //keyword static
        //maka dia tidak perlu diakses melalui objek, tapi dari
        //kelasnya langsung sudah bisa
    }
}