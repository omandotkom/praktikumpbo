//Source code untuk bagian 2.3.2 Java Modifier
public class Manusia{
    private String nama;
    private int umur;

    //coba ini nanti dijadiin private
    public void setNama(String a){
        nama = a;
    }
    public String getNama(){
        return nama;
    }
    public void setUmur(int a){
        umur = a;
    }
    public int getUmur(){
        return umur;
    }

}