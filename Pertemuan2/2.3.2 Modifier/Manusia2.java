//Source code untuk bagian 2.3.2 Java Modifier
public class Manusia2{
    public static void main(String[] args){
        //Membuat objek Manusia bernama hendra
        Manusia hendra = new Manusia();
        //Mengatur nama dengan memanggil setNama
        hendra.setNama("Hendra Agustinus");
        //Mengatur nama dengan memanggil setUmur
        hendra.setUmur(20);

        String namaHendra = hendra.getNama();
        int umurHendra = hendra.getUmur();

        System.out.println("Nama : " + namaHendra);
        System.out.println("Umur : " + umurHendra);
    }
}