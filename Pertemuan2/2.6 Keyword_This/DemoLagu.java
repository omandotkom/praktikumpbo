//Source code untuk bagian 2.6 Keyword "this"
class Lagu{
    private String pencipta,judul;

    public void isiParam(String judul, String pencipta){
        this.judul = judul;
        this.pencipta = pencipta;
    }
    public void cetakKeLayar(){
        if (judul==null && pencipta == null )return;

        System.out.println("Judul : " + judul + ", pencipta : " + pencipta);
    }
}
public class DemoLagu{
    public static void main(String[] args){
        Lagu a = new Lagu();
        a.isiParam("God will make a way", "Don Moen");
        a.cetakKeLayar();
    }
}